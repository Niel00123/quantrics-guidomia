package com.example.guidomia

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.guidomia.adapter.CustomAdapter
import com.example.guidomia.api.ResponseItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.Locale
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private final var FILE_NAME = "localDB"
    private lateinit var recyclerView: RecyclerView
    private lateinit var imageView: ImageView
    private lateinit var myToolbar: androidx.appcompat.widget.Toolbar
    private lateinit var adapter: CustomAdapter
    private lateinit var txtSearchMake: TextView
    private lateinit var txtModel: TextView
    private lateinit var responseItem: List<ResponseItem>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtSearchMake = findViewById(R.id.txtSearchMake)
        txtModel = findViewById(R.id.txtModel)
        recyclerView = findViewById(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        imageView = findViewById(R.id.homeImage)
        myToolbar = findViewById(R.id.myToolbar)

        setSupportActionBar(myToolbar)

        val drawableResourceId =
            this.resources.getIdentifier("tacoma", "drawable", this.packageName)

        loadImage(drawableResourceId)
        loadJson()
        setFilter()

    }

    private fun loadImage(drawableResourceId: Int) {
        Glide.with(this)
            .load(drawableResourceId)
            .into(imageView)

    }

    private fun loadJson() {
        val gson = Gson()
        val listResponse = object : TypeToken<List<ResponseItem>>() {}.type
        if (!fileExist()) {
            val jsonFileString = getJsonDataFromAsset(applicationContext, "cars.json")
            responseItem = gson.fromJson(jsonFileString, listResponse)
            val file = File(applicationContext.getFilesDir(), FILE_NAME)
            val fileWriter = FileWriter(file)
            val bufferedWriter = BufferedWriter(fileWriter)
            bufferedWriter.write(jsonFileString.toString())
            bufferedWriter.close()
        } else {
            val file = File(applicationContext.filesDir, FILE_NAME)
            val bytes = file.readBytes()
            val str = bytes.toString(Charsets.UTF_8)
            responseItem = gson.fromJson(str, listResponse)
        }
        adapter = CustomAdapter(applicationContext, responseItem)
        recyclerView.adapter = adapter
        recyclerView.apply {
            var itemDecoration = DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(getDrawable(R.drawable.divider)!!)
            addItemDecoration(itemDecoration)
        }
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    private fun fileExist(): Boolean {
        val file = File(applicationContext.filesDir, FILE_NAME)
        var fileExists = file.exists()
        return fileExists
    }

    private fun setFilter() {
        txtSearchMake.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                filterMake(editable.toString())
            }
        })

        txtModel.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                filterModel(editable.toString())
            }
        })
    }

    private fun filterMake(text: String) {
        val filteredResponseItem = ArrayList<ResponseItem>()
        responseItem.filterTo(filteredResponseItem) {
            it.make?.lowercase(Locale.getDefault())?.contains(text.lowercase(Locale.getDefault())) == true
        }
        adapter.filterList(filteredResponseItem)
    }

    private fun filterModel(text: String) {
        val filteredResponseItem = ArrayList<ResponseItem>()
        responseItem.filterTo(filteredResponseItem) {
            it.model?.lowercase(Locale.getDefault())?.contains(text.lowercase(Locale.getDefault())) == true
        }
        adapter.filterList(filteredResponseItem)
    }

}