package com.example.guidomia.api

import com.google.gson.annotations.SerializedName

data class Response(

	@field:SerializedName("Response")
	val response: List<ResponseItem?>? = null
)

data class ResponseItem(

	@field:SerializedName("marketPrice")
	val marketPrice: Double? = null,

	@field:SerializedName("prosList")
	val prosList: List<String?>? = null,

	@field:SerializedName("rating")
	val rating: Int? = null,

	@field:SerializedName("consList")
	val consList: List<String?>? = null,

	@field:SerializedName("model")
	val model: String? = null,

	@field:SerializedName("customerPrice")
	val customerPrice: Double? = null,

	@field:SerializedName("make")
	val make: String? = null
)
