package com.example.guidomia.adapter

import ConsAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.example.guidomia.R
import com.example.guidomia.api.ResponseItem
import androidx.recyclerview.widget.LinearLayoutManager

private var mExpandedPosition: Int = 0
private val viewPool = RecyclerView.RecycledViewPool()

private lateinit var prosRecyclerView: RecyclerView
private lateinit var consRecyclerView: RecyclerView

class CustomAdapter(private var context: Context, var responseItem: List<ResponseItem>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_group, parent, false)

        prosRecyclerView = view.findViewById(R.id.prosRecyclerView)
        consRecyclerView = view.findViewById(R.id.consRecyclerView)

        return ViewHolder(view)
    }

    @SuppressLint("WrongConstant")
    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val isExpanded = position == mExpandedPosition
        holder.expandableLinear.visibility = if (isExpanded) View.VISIBLE else View.GONE
        holder.itemView.isActivated = isExpanded
        var previousExpandedPosition = mExpandedPosition
        if (isExpanded) previousExpandedPosition = position

        holder.itemView.setOnClickListener {
            mExpandedPosition = if (isExpanded) -1 else position

            notifyItemChanged(previousExpandedPosition)
            notifyItemChanged(position)
        }

        val responseItemPosition = responseItem[position]
        when {
            responseItemPosition.make.equals("Land Rover") -> {
                holder.imageView.setImageResource(R.drawable.range_rover)
            }
            responseItemPosition.make.equals("Alpine") -> {
                holder.imageView.setImageResource(R.drawable.alpine_roadster)
            }
            responseItemPosition.make.equals("BMW") -> {
                holder.imageView.setImageResource(R.drawable.bmw_330i)
            }
            else -> {
                holder.imageView.setImageResource(R.drawable.mercedez_benz_glc)
            }
        }

        holder.carMake.text = responseItemPosition.make
        holder.carPrice.text = context.resources.getString(R.string.price,  responseItemPosition.marketPrice?.toInt())
        holder.rating.rating = responseItemPosition.rating?.toFloat()!!

        val prosLayoutManager =
            LinearLayoutManager(prosRecyclerView.context, LinearLayout.VERTICAL, false)
        prosRecyclerView.apply {
            layoutManager = prosLayoutManager
            val adapter = ProsAdapter(responseItemPosition.prosList as List<String>)
            holder.prosRecyclerView.adapter = adapter
            setRecycledViewPool(viewPool)
        }

        val consLayoutManager =
            LinearLayoutManager(consRecyclerView.context, LinearLayoutManager.VERTICAL, false)
        consRecyclerView.apply {
            layoutManager = consLayoutManager
            val adapter = ConsAdapter(responseItemPosition.consList as List<String>)
            holder.consRecyclerView.adapter = adapter
            setRecycledViewPool(viewPool)
        }
    }

    override fun getItemCount(): Int {
        return responseItem.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: ImageView = itemView.findViewById(R.id.homeImage)
        val carMake: TextView = itemView.findViewById(R.id.carMake)
        val carPrice: TextView = itemView.findViewById(R.id.carPrice)
        val rating: RatingBar = itemView.findViewById(R.id.rating)
        val expandableLinear: LinearLayout = itemView.findViewById(R.id.expandableLinear)
        val prosRecyclerView: RecyclerView = itemView.findViewById(R.id.prosRecyclerView)
        val consRecyclerView: RecyclerView = itemView.findViewById(R.id.consRecyclerView)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun filterList(responseItem: List<ResponseItem>) {
        this.responseItem = responseItem
        notifyDataSetChanged()
    }
}