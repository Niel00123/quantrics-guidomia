import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.guidomia.R

class ConsAdapter(
    private val consItemList: List<String>

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_cons, parent, false))
    }

    override fun getItemCount(): Int {
        return consItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.txtString).text = consItemList[position]
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
